<?php

namespace App;


class ToitOuvrantDecorator implements IVehiculeOptionnable
{
    private $VehiculeAvecOption;
    private $prixOption;

    public function __construct(IVehiculeOptionnable $VehiculeAvecOption, $prixOption)
    {
        $this->VehiculeAvecOption = $VehiculeAvecOption;
        $this->prixOption = $prixOption;
    }

    public function getVehiculeAvecOption()
    {
        return $this->VehiculeAvecOption->getVehiculeAvecOption() . ' - Toit Ouvrant ' . $this->prixOption . "euros";
    }

    public function getPrice()
    {
        return $this->prixOption + $this->VehiculeAvecOption->getPrice();
    }

}