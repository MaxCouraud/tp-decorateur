<?php

namespace App;

use App\Vehicule;

class VehiculeSansOption extends Vehicule
{

    public function __construct($prixBasique)
    {
        parent::__construct($prixBasique);
    }

    public function getVehiculeSansOption()
    {
        return 'Le produit choisis est une voiture avec options.';
    }
}