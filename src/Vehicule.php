<?php

namespace App;


abstract class Vehicule
{
    private $prixBasique;

    public function __construct($prixBasique)
    {
        $this->prixBasique = $prixBasique;
    }

    public function __toString()
    {
        return $this->prixBasique;
    }

    public function getPrixBasique()
    {
        return $this->prixBasique;
    }
}