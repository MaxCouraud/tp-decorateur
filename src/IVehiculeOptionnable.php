<?php

namespace App;


interface IVehiculeOptionnable
{
    public function getVehiculeAvecOption();
    public function getPrice();
}