<?php

namespace App;

use App\Vehicule;

class VehiculeAvecOption extends Vehicule implements IVehiculeOptionnable
{

    public function __construct($prixBasique)
    {
        parent::__construct($prixBasique);
    }

    public function getVehiculeAvecOption()
    {
        return 'Le produit choisis est une voiture avec options.';
    }

    public function getPrice()
    {
        return $this->getPrixBasique();
    }
}