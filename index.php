<?php 

require __DIR__ . "/vendor/autoload.php";


$vehiculeAvecOption = new App\VehiculeAvecOption(10000);
$vehiculeAvecOption = new App\JanteAluDecorator($vehiculeAvecOption, 600);
$vehiculeAvecOption = new App\PeintureDecorator($vehiculeAvecOption, 800);
$vehiculeAvecOption = new App\ToitOuvrantDecorator($vehiculeAvecOption, 1000);

echo $vehiculeAvecOption->getVehiculeAvecOption() . " </br> Son prix totale est égale à : ";
echo $vehiculeAvecOption->getPrice();

